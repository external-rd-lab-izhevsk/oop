﻿using System;
using NUnit.Framework;
using Oop.Shapes.Factories;

namespace Oop.Shapes.Tests
{
	[TestFixture]
	public class TriangleTests
	{
		private ShapeFactory _shapeFactory;

		[OneTimeSetUp]
		public void FixtureSrtUp()
		{
			_shapeFactory = new ShapeFactory();
		}

		[TestCase(0, 0, 0)]
		[TestCase(-1, -1, -1)]
		[TestCase(int.MinValue, int.MinValue, int.MinValue)]
		[TestCase(-1, 0, 1)]
		[TestCase(1, 0, -1)]
		[TestCase(0, 1, -1)]
		[TestCase(0, -1, 1)]
		public void Constructor_OneOrMoreSideIsInvalid_ThrowsArgumentOutOfRangeException(int a, int b, int c)
		{
			Assert.Throws<ArgumentOutOfRangeException>(() =>
			{
				Shape _ = _shapeFactory.CreateTriangle(a, b, c);
			});
		}

		[TestCase(1, 2, 10)]
		[TestCase(10, 3, 6)]
		[TestCase(10, 13, 2)]
		public void Constructor_SidesRatioIsInvalid_ThrowsInvalidOperationException(int a, int b, int c)
		{
			Assert.Throws<InvalidOperationException>(() =>
			{
				Shape _ = _shapeFactory.CreateTriangle(a, b, c);
			});
		}

		[TestCase(1, 1, 1, ExpectedResult = 0.433)]
		[TestCase(5, 6, 7, ExpectedResult = 14.6969)]
		[TestCase(12, 21, 20, ExpectedResult = 117.2047)]
		public decimal Area___ReturnsExpectedValue(int a, int b, int c)
		{
			Shape triangle = _shapeFactory.CreateTriangle(a, b, c);
			var area = triangle.Area;

			return (decimal)Math.Round(area, 4);
		}

		[TestCase(1, 1, 1, ExpectedResult = 3)]
		[TestCase(10, 15, 20, ExpectedResult = 45)]
		[TestCase(3, 5, 7, ExpectedResult = 15)]
		public decimal Perimeter___ReturnsExpectedValue(int a, int b, int c)
		{
			Shape triangle = _shapeFactory.CreateTriangle(a, b, c);
			var perimeter = triangle.Perimeter;

			return (decimal)Math.Round(perimeter, 0);
		}

		[Test]
		public void VertexCount___Returns3()
		{
			Shape triangle = _shapeFactory.CreateTriangle(1, 1, 1);
			var vertexCount = triangle.VertexCount;

			Assert.AreEqual(3, vertexCount);
		}

		[TestCase(4, 5, 6, ExpectedResult = true)]
		[TestCase(4, 6, 5, ExpectedResult = true)]
		[TestCase(5, 4, 6, ExpectedResult = true)]
		[TestCase(5, 6, 4, ExpectedResult = true)]
		[TestCase(6, 4, 5, ExpectedResult = true)]
		[TestCase(6, 5, 4, ExpectedResult = true)]
		[TestCase(4, 5, 7, ExpectedResult = false)]
		[TestCase(3, 5, 6, ExpectedResult = false)]
		[TestCase(4, 3, 6, ExpectedResult = false)]
		public bool IsEqual_OtherTriangle_ReturnsExpectedResult(int a, int b, int c)
		{
			var triangle1 = _shapeFactory.CreateTriangle(4, 5, 6);
			var triangle2 = _shapeFactory.CreateTriangle(a, b, c);

			return triangle1.IsEqual(triangle2);
		}

		[Test]
		public void IsEqual_Circle_ReturnsFalse()
		{
			var triangle = _shapeFactory.CreateTriangle(4, 5, 6);
			var circle = _shapeFactory.CreateCircle(4);

			var isEqual = triangle.IsEqual(circle);

			Assert.False(isEqual);
		}

		[Test]
		public void IsEqual_Square_ReturnsFalse()
		{
			var triangle = _shapeFactory.CreateTriangle(4, 5, 6);
			var square = _shapeFactory.CreateSquare(4);

			var isEqual = triangle.IsEqual(square);

			Assert.False(isEqual);
		}

		[Test]
		public void IsEqual_Rectangle_ReturnsFalse()
		{
			var triangle = _shapeFactory.CreateTriangle(4, 5, 6);
			var rectangle = _shapeFactory.CreateRectangle(4, 5);

			var isEqual = triangle.IsEqual(rectangle);

			Assert.False(isEqual);
		}

	}
}